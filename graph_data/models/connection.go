package models

import "github.com/jinzhu/gorm"

type Connection struct {
	gorm.Model
	NodeFromID uint
	NodeFrom   *Node `json:"-" gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	NodeToID   uint
	NodeTo     *Node `json:"-" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	Value      int
}

func (c *Connection) PrepareAndValid(nodes map[uint]*Node) (ok bool) {
	if c.Value < 0 {
		return false
	}
	nodeFrom, ok := nodes[c.NodeFromID]
	if !ok {
		return false
	}
	c.NodeFrom = nodeFrom
	nodeTo, ok := nodes[c.NodeFromID]
	if !ok {
		return false
	}
	c.Model = gorm.Model{}
	c.NodeFrom = nodeFrom
	c.NodeTo = nodeTo

	return true
}
