package models

import "github.com/jinzhu/gorm"

type Result struct {
	gorm.Model
	GraphID uint
	NodeID  uint
	Node    *Node `json:",omitempty"`
	Value   int
}
