package models

import (
	"gorm.io/gorm"
)

type Graph struct {
	gorm.Model
	UserID      uint
	Nodes       []*Node
	StartNodeID uint
	StartNode   *Node     `json:"-"`
	SentToSolve bool      `json:"-"`
	IsResult    bool      `json:"-"`
	Results     []*Result `json:",omitempty"`
}

func (g *Graph) PrepareAndValid() (ok bool) {
	if g.UserID == 0 {
		return false
	}
	if len(g.Nodes) > 0 {
		nodes := make(map[uint]*Node)
		var connections []*Connection
		for _, n := range g.Nodes {
			if n.ID <= 0 {
				return false
			}
			nodes[n.ID] = n
			if !n.PrepareAndValid() {
				return false
			}
			connections = append(connections, n.ConnectionOutgoing...)
		}
		for _, c := range connections {
			if !c.PrepareAndValid(nodes) {
				return false
			}
		}

		startNode, ok := nodes[g.StartNodeID]
		if !ok {
			return false
		}
		g.StartNode = startNode

	}
	return true
}
