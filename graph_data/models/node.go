package models

import "github.com/jinzhu/gorm"

type Node struct {
	gorm.Model
	GraphID            uint
	Name               string
	ConnectionOutgoing []*Connection `gorm:"foreignKey:NodeFromID"`
}

//check only Node field without ConnectionOutGoing
func (n *Node) PrepareAndValid() (ok bool) {
	if n.Name == "" {
		return false
	}
	n.Model = gorm.Model{}
	n.GraphID = 0
	return true
}
