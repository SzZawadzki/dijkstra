package repositories

import (
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	e "gitlab.com/SzZawadzki/dijkstra/error"
)

type todo struct {
	todoChannel *amqp.Channel
	todoQueue   amqp.Queue
}

type Todo interface {
	Publish(graphID string) error
}

func NewTodo(todoChannel *amqp.Channel,
	todoQueue amqp.Queue) Todo {
	return &todo{todoChannel: todoChannel, todoQueue: todoQueue}
}

func (t *todo) Publish(graphId string) error {
	err := t.todoChannel.Publish(
		"", //?
		t.todoQueue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(graphId),
		},
	)
	if err != nil {
		e.NewWithCode(e.InternalError, errors.WithStack(err))
	}
	return nil
}
