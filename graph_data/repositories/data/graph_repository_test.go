package repositories_test

import (
	"os"
	"testing"

	e "gitlab.com/SzZawadzki/dijkstra/error"

	"github.com/stretchr/testify/require"
	"gitlab.com/SzZawadzki/dijkstra/graph_data/database"
	"gitlab.com/SzZawadzki/dijkstra/graph_data/models"
	"gitlab.com/SzZawadzki/dijkstra/graph_data/repositories"
	"gorm.io/gorm"
)

var DB *gorm.DB
var repo repositories.GraphRepository
var userID = 1
var graph models.Graph

func TestMain(m *testing.M) {
	DB = database.GetConn("postgres", "mysecretpassword", "localhost", "data_test")
	DB.Exec("delete from connections")
	DB.Exec("delete from results")
	DB.Exec("delete from nodes")
	DB.Exec("delete from graphs")
	repo = repositories.New(DB)
	os.Exit(m.Run())
}

func TestCreateGraphWithID(t *testing.T) {
	assertions := require.New(t)

	id := 12

	graphOrginal := &models.Graph{UserID: uint(userID)}

	graphOrginal.ID = uint(id)

	graphOrginal2 := &models.Graph{UserID: uint(userID)}

	graphOrginal2.ID = uint(id)

	repo.CreateGraph(graphOrginal)

	err := repo.CreateGraph(graphOrginal2)

	assertions.NoError(err, "Create error")

	repo.DeleteGraph(graphOrginal.ID)
	repo.DeleteGraph(graphOrginal2.ID)
}

func TestCreateGraph(t *testing.T) {
	assertions := require.New(t)

	n1 := &models.Node{Name: "1"}
	n2 := &models.Node{Name: "2"}
	n3 := &models.Node{Name: "3"}
	n4 := &models.Node{Name: "4"}
	n5 := &models.Node{Name: "5"}
	n6 := &models.Node{Name: "6"}
	n7 := &models.Node{Name: "7"}
	c12 := &models.Connection{NodeFrom: n1, NodeTo: n2, Value: 4}
	c21 := &models.Connection{NodeFrom: n2, NodeTo: n1, Value: 5}
	c13 := &models.Connection{NodeFrom: n1, NodeTo: n3, Value: 8}
	c34 := &models.Connection{NodeFrom: n3, NodeTo: n4, Value: 2}
	c36 := &models.Connection{NodeFrom: n3, NodeTo: n6, Value: 8}
	c25 := &models.Connection{NodeFrom: n2, NodeTo: n5, Value: 6}
	c47 := &models.Connection{NodeFrom: n4, NodeTo: n7, Value: 3}
	c45 := &models.Connection{NodeFrom: n4, NodeTo: n5, Value: 4}
	c53 := &models.Connection{NodeFrom: n5, NodeTo: n3, Value: 3}
	c56 := &models.Connection{NodeFrom: n5, NodeTo: n6, Value: 3}
	c62 := &models.Connection{NodeFrom: n6, NodeTo: n2, Value: 10}
	c72 := &models.Connection{NodeFrom: n7, NodeTo: n2, Value: 8}
	c76 := &models.Connection{NodeFrom: n7, NodeTo: n7, Value: 4}
	n1.ConnectionOutgoing = []*models.Connection{c12, c13}
	n2.ConnectionOutgoing = []*models.Connection{c21, c25}
	n3.ConnectionOutgoing = []*models.Connection{c34, c36}
	n4.ConnectionOutgoing = []*models.Connection{c45, c47}
	n5.ConnectionOutgoing = []*models.Connection{c53, c56}
	n6.ConnectionOutgoing = []*models.Connection{c62}
	n7.ConnectionOutgoing = []*models.Connection{c72, c76}
	graphOrginal := &models.Graph{UserID: uint(userID), Nodes: []*models.Node{n1, n2, n3, n4, n5, n6, n7}, StartNode: n1}

	repo.CreateGraph(graphOrginal)

	graphsReturned, _ := repo.GetGraphsForUser(uint(userID))

	assertions.Equal(1, len(graphsReturned), "Len of graphs")

	graphReturned := graphsReturned[0]

	graph = *graphReturned

	assertions.Equal(graphOrginal.ID, graphReturned.ID, "Graph Ids")

	assertions.Equal(7, len(graphReturned.Nodes), "Len nodes")

	var testNode6 *models.Node
	for i := range graphReturned.Nodes {
		if graphReturned.Nodes[i].Name == "6" {
			testNode6 = graphReturned.Nodes[i]
			break
		}
	}
	assertions.Equal(1, len(testNode6.ConnectionOutgoing), "Len connections")

	// bytea, err := json.Marshal(graphOrginal)
	// if err != nil {
	// 	t.Log(err)
	// }
	//t.Log(string(bytea))
}

func TestAddResult(t *testing.T) {
	assertions := require.New(t)
	assertions.NotNil(graph, "Run all test")

	result := make([]*models.Result, len(graph.Nodes))
	for i, n := range graph.Nodes {
		result[i] = &models.Result{
			GraphID: graph.ID,
			NodeID:  n.ID,
			Node:    n,
			Value:   i}
	}

	err := repo.AddResultToGraph(result)

	assertions.NoError(err, "Error Add result")

	graphs, err := repo.GetGraphsForUser(uint(userID))

	assertions.NoError(err, "Error Get graph")

	vargraph := graphs[0]

	assertions.True(vargraph.IsResult, "graph variable isResult")

	assertions.Equal(len(result), len(vargraph.Results), "Len of result")

	for _, r := range vargraph.Results {
		var checkR *models.Result
		for _, varR := range result {
			if r.NodeID == varR.NodeID {
				checkR = varR
				break
			}
		}
		if checkR == nil || checkR.Value != r.Value || r.GraphID != vargraph.ID {
			t.Errorf("Result is incorrect; There is NodeID: %d Value: %d GraphID %d, SHOULD be NodeID: %d Value: %d GraphID %d.",
				r.NodeID, r.Value, r.GraphID, checkR.NodeID, checkR.Value, checkR.GraphID)
		}
	}
}

func TestSetIsSentToSolve(t *testing.T) {
	assertions := require.New(t)
	assertions.NotNil(graph, "Run all test")

	err := repo.SetIsSentToSolve(graph.ID)
	assertions.NoError(err, "Set is sent to solve error")

	graphT, _ := repo.GetGraphByID(graph.ID)
	graph = *graphT

	assertions.True(graph.SentToSolve, "graph variable sentToSolve")
}

func TestUpdateGraph(t *testing.T) {
	assertions := require.New(t)
	var graphOrginal *models.Graph
	{
		n1 := &models.Node{Name: "1"}
		n2 := &models.Node{Name: "2"}
		n3 := &models.Node{Name: "3"}
		n4 := &models.Node{Name: "4"}
		n5 := &models.Node{Name: "5"}
		n6 := &models.Node{Name: "6"}
		n7 := &models.Node{Name: "7"}
		c12 := &models.Connection{NodeFrom: n1, NodeTo: n2, Value: 4}
		c21 := &models.Connection{NodeFrom: n2, NodeTo: n1, Value: 5}
		c13 := &models.Connection{NodeFrom: n1, NodeTo: n3, Value: 8}
		c34 := &models.Connection{NodeFrom: n3, NodeTo: n4, Value: 2}
		c36 := &models.Connection{NodeFrom: n3, NodeTo: n6, Value: 8}
		c25 := &models.Connection{NodeFrom: n2, NodeTo: n5, Value: 6}
		c47 := &models.Connection{NodeFrom: n4, NodeTo: n7, Value: 3}
		c45 := &models.Connection{NodeFrom: n4, NodeTo: n5, Value: 4}
		c53 := &models.Connection{NodeFrom: n5, NodeTo: n3, Value: 3}
		c56 := &models.Connection{NodeFrom: n5, NodeTo: n6, Value: 3}
		c62 := &models.Connection{NodeFrom: n6, NodeTo: n2, Value: 10}
		c72 := &models.Connection{NodeFrom: n7, NodeTo: n2, Value: 8}
		c76 := &models.Connection{NodeFrom: n7, NodeTo: n7, Value: 4}
		n1.ConnectionOutgoing = []*models.Connection{c12, c13}
		n2.ConnectionOutgoing = []*models.Connection{c21, c25}
		n3.ConnectionOutgoing = []*models.Connection{c34, c36}
		n4.ConnectionOutgoing = []*models.Connection{c45, c47}
		n5.ConnectionOutgoing = []*models.Connection{c53, c56}
		n6.ConnectionOutgoing = []*models.Connection{c62}
		n7.ConnectionOutgoing = []*models.Connection{c72, c76}
		graphOrginal = &models.Graph{UserID: uint(userID), Nodes: []*models.Node{n1, n2, n3, n4, n5, n6, n7}, StartNode: n1}

		repo.CreateGraph(graphOrginal)
	}
	graphToUpdate := *graphOrginal

	n1 := &models.Node{Name: "1"}
	n2 := &models.Node{Name: "2"}
	n3 := &models.Node{Name: "3"}
	n4 := &models.Node{Name: "4"}
	n5 := &models.Node{Name: "5"}
	n6 := &models.Node{Name: "6"}
	n7 := &models.Node{Name: "7"}
	n8 := &models.Node{Name: "8"}
	c12 := &models.Connection{NodeFrom: n1, NodeTo: n2, Value: 4}
	c21 := &models.Connection{NodeFrom: n2, NodeTo: n1, Value: 5}
	c13 := &models.Connection{NodeFrom: n1, NodeTo: n3, Value: 8}
	c34 := &models.Connection{NodeFrom: n3, NodeTo: n4, Value: 2}
	c36 := &models.Connection{NodeFrom: n3, NodeTo: n6, Value: 8}
	c25 := &models.Connection{NodeFrom: n2, NodeTo: n5, Value: 6}
	c47 := &models.Connection{NodeFrom: n4, NodeTo: n7, Value: 3}
	c51 := &models.Connection{NodeFrom: n5, NodeTo: n1, Value: 4}
	c53 := &models.Connection{NodeFrom: n5, NodeTo: n3, Value: 3}
	c56 := &models.Connection{NodeFrom: n5, NodeTo: n6, Value: 3}
	c62 := &models.Connection{NodeFrom: n6, NodeTo: n2, Value: 10}
	c72 := &models.Connection{NodeFrom: n7, NodeTo: n2, Value: 8}
	c76 := &models.Connection{NodeFrom: n7, NodeTo: n7, Value: 4}
	c82 := &models.Connection{NodeFrom: n8, NodeTo: n2, Value: 8}
	c81 := &models.Connection{NodeFrom: n8, NodeTo: n1, Value: 4}
	n1.ConnectionOutgoing = []*models.Connection{c12, c13}
	n2.ConnectionOutgoing = []*models.Connection{c21, c25}
	n3.ConnectionOutgoing = []*models.Connection{c34, c36}
	n4.ConnectionOutgoing = []*models.Connection{c47}
	n5.ConnectionOutgoing = []*models.Connection{c53, c56, c51}
	n6.ConnectionOutgoing = []*models.Connection{c62}
	n7.ConnectionOutgoing = []*models.Connection{c72, c76}
	n8.ConnectionOutgoing = []*models.Connection{c82, c81}

	graphToUpdate.Nodes = []*models.Node{n1, n2, n3, n4, n5, n6, n7, n8}
	graphToUpdate.StartNode = n3

	err := repo.UpdateGraph(&graphToUpdate)

	assertions.NoError(err, "Update method error")

	graphTest, err := repo.GetGraphByID(graphOrginal.ID)

	assertions.NoError(err, "get graph by ID error")

	assertions.Equal(8, len(graphTest.Nodes), "Quantity notes")

	var checkNode *models.Node
	for i := range graphTest.Nodes {
		if graphTest.Nodes[i].Name == "5" {
			checkNode = graphTest.Nodes[i]
			break
		}
	}

	assertions.Equal(3, len(checkNode.ConnectionOutgoing), "Len of connectionOutGoing node 5")
}

func TestGetGraphByIdWithoutPreloading(t *testing.T) {
	assertions := require.New(t)
	assertions.NotNil(graph, "Run all test")

	graphPreloaded, errP := repo.GetGraphByID(graph.ID)
	graph, err := repo.GetGraphByIDWithoutPreloading(graph.ID)

	assertions.NoError(errP, "Error get by id")
	assertions.NoError(err, "Error get by id wothout preloading")
	assertions.NotNil(graphPreloaded, "graph preloaded")
	assertions.NotNil(graph)

	assertions.Equal(graphPreloaded.IsResult, graph.IsResult, "is sresult")
	assertions.Equal(graphPreloaded.UserID, graph.UserID, "userID")
	assertions.Equal(graphPreloaded.StartNodeID, graph.StartNodeID, "stardt node id")
	assertions.Equal(graphPreloaded.SentToSolve, graph.SentToSolve, "sent to solve")
}

func TestDeleteGraph(t *testing.T) {
	assertions := require.New(t)
	assertions.NotNil(graph, "Run all test")
	err := repo.DeleteGraph(graph.ID)

	assertions.NoError(err, "delete graph err")

	g, err := repo.GetGraphByID(graph.ID)

	errAs := err.(*e.Error)

	assertions.Equal(errAs.Code, e.NotFound, "get graph by Id")

	assertions.Nil(g, "getted graph")
}

func TestDeleteGraphNoConnections(t *testing.T) {
	assertions := require.New(t)
	n1 := &models.Node{Name: "1"}
	n2 := &models.Node{Name: "2"}
	n3 := &models.Node{Name: "3"}
	n4 := &models.Node{Name: "4"}
	graph := &models.Graph{UserID: uint(userID), Nodes: []*models.Node{n1, n2, n3, n4}, StartNode: n1}

	err := repo.CreateGraph(graph)
	assertions.NoError(err, "Create graph err")

	err = repo.DeleteGraph(graph.ID)

	assertions.NoError(err, "Delete graph error")

}

func TestDeleteGraphNoNodes(t *testing.T) {
	assertions := require.New(t)
	graph := &models.Graph{UserID: uint(userID)}

	err := repo.CreateGraph(graph)
	assertions.NoError(err, "Create graph err")

	err = repo.DeleteGraph(graph.ID)

	assertions.NoError(err, "Delete graph error")

}
