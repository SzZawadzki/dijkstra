package repositories

import (
	"github.com/pkg/errors"
	e "gitlab.com/SzZawadzki/dijkstra/error"

	"gitlab.com/SzZawadzki/dijkstra/graph_data/models"
	"gorm.io/gorm"
)

type dataRepository struct {
	db *gorm.DB
}

type DataRepository interface {
	CreateGraph(graph *models.Graph) error
	GetGraphsForUser(userID uint) ([]*models.Graph, error)
	GetGraphByID(graphID uint) (*models.Graph, error)
	AddResultToGraph(result []*models.Result) error
	SetIsSentToSolve(graphID uint) error
	UpdateGraph(graph *models.Graph) error
	DeleteGraph(graphID uint) error
	GetGraphByIDWithoutPreloading(graphID uint) (*models.Graph, error)
}

func NewData(DB *gorm.DB) DataRepository {
	return &dataRepository{DB}
}

func (r *dataRepository) CreateGraph(graph *models.Graph) error {
	graph.ID = 0
	graph.IsResult = false
	graph.SentToSolve = false
	var connections []*models.Connection
	nodes := graph.Nodes
	graph.Nodes = nil
	startNode := graph.StartNode
	graph.StartNodeID = 0
	graph.StartNode = nil
	graph.Results = nil
	err := r.db.Create(graph).Error
	if err != nil {
		return e.NewWithCode(e.InternalError, errors.WithStack(err))
	}

	for _, n := range nodes {
		n.ID = 0
		varConOut := n.ConnectionOutgoing
		n.ConnectionOutgoing = nil
		n.GraphID = graph.ID
		err = r.db.Create(n).Error
		if err != nil {
			return e.NewWithCode(e.InternalError, errors.WithStack(err))
		}
		n.ConnectionOutgoing = varConOut
		connections = append(connections, varConOut...)
	}

	for _, c := range connections {
		c.ID = 0
		varNodeFrom := c.NodeFrom
		varNodeTo := c.NodeTo
		c.NodeFromID = c.NodeFrom.ID
		c.NodeToID = c.NodeTo.ID
		c.NodeFrom = nil
		c.NodeTo = nil
		err = r.db.Create(c).Error
		if err != nil {
			return e.NewWithCode(e.InternalError, errors.WithStack(err))
		}
		c.NodeFrom = varNodeFrom
		c.NodeTo = varNodeTo
	}

	if startNode != nil {
		graph.StartNodeID = startNode.ID
		err = r.db.Save(graph).Error
		if err != nil {
			return e.NewWithCode(e.InternalError, errors.WithStack(err))
		}
	}

	graph.Nodes = nodes
	graph.StartNode = startNode

	return nil
}

func (r *dataRepository) GetGraphsForUser(userID uint) ([]*models.Graph, error) {
	var graphs []*models.Graph
	err := r.db.Preload("Nodes").Preload("Nodes.ConnectionOutgoing").Preload("Results").Find(&graphs, "graphs.user_id = ?", userID).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, e.NewWithCode(e.NotFound, errors.WithStack(err))
		}
		return nil, e.NewWithCode(e.InternalError, errors.WithStack(err))
	}
	return graphs, nil
}

func (r *dataRepository) AddResultToGraph(results []*models.Result) error {
	for _, res := range results {
		if err := r.db.Create(res).Error; err != nil {
			return e.NewWithCode(e.InternalError, errors.WithStack(err))
		}
	}

	err := r.db.Model(&models.Graph{}).Where("id = ?", results[0].GraphID).Update("is_result", "true").Error
	if err != nil {
		return e.NewWithCode(e.InternalError, errors.WithStack(err))
	}
	return nil
}

func (r *dataRepository) GetGraphByID(graphID uint) (*models.Graph, error) {
	var graph *models.Graph
	err := r.db.Preload("Nodes").Preload("Nodes.ConnectionOutgoing").Preload("Results").First(&graph, graphID).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, e.NewWithCode(e.NotFound, errors.WithStack(err))
		}
		return nil, e.NewWithCode(e.InternalError, errors.WithStack(err))
	}
	return graph, nil
}

func (r *dataRepository) SetIsSentToSolve(graphID uint) error {
	err := r.db.Model(&models.Graph{}).Where("id = ?", graphID).Update("sent_to_solve", "true").Error
	if err != nil {
		return e.NewWithCode(e.InternalError, errors.WithStack(err))
	}
	return nil
}

func (r *dataRepository) UpdateGraph(graph *models.Graph) error {
	var nodesToDelete []models.Node
	err := r.db.Where("graph_id = ?", graph.ID).Find(&nodesToDelete).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return e.NewWithCode(e.InternalError, errors.WithStack(err))
	}

	for _, n := range nodesToDelete {
		err := r.db.Where("node_from_id = ?", n.ID).Delete(models.Connection{}).Error
		if err != nil {
			return e.NewWithCode(e.InternalError, errors.WithStack(err))
		}
	}
	for _, n := range nodesToDelete {
		err := r.db.Delete(&n).Error
		if err != nil {
			return e.NewWithCode(e.InternalError, errors.WithStack(err))
		}
	}

	var connectionsToAdd []*models.Connection
	nodesToAdd := graph.Nodes
	graph.Nodes = nil

	for _, n := range nodesToAdd {
		varConOut := n.ConnectionOutgoing
		n.ConnectionOutgoing = nil
		n.GraphID = graph.ID
		err = r.db.Create(n).Error
		if err != nil {
			return e.NewWithCode(e.InternalError, errors.WithStack(err))
		}
		n.ConnectionOutgoing = varConOut
		connectionsToAdd = append(connectionsToAdd, varConOut...)
	}

	for _, c := range connectionsToAdd {
		varNodeFrom := c.NodeFrom
		varNodeTo := c.NodeTo
		c.NodeFromID = c.NodeFrom.ID
		c.NodeToID = c.NodeTo.ID
		c.NodeFrom = nil
		c.NodeTo = nil
		err = r.db.Create(c).Error
		if err != nil {
			return e.NewWithCode(e.InternalError, errors.WithStack(err))
		}
		c.NodeFrom = varNodeFrom
		c.NodeTo = varNodeTo
	}

	startNode := graph.StartNode
	graph.StartNode = nil
	graph.StartNodeID = startNode.ID
	err = r.db.Save(graph).Error
	if err != nil {
		return e.NewWithCode(e.InternalError, errors.WithStack(err))
	}
	graph.Nodes = nodesToAdd
	graph.StartNode = startNode

	return nil
}

func (r *dataRepository) DeleteGraph(graphID uint) error {
	var nodesToDelete []models.Node
	err := r.db.Where("graph_id = ?", graphID).Find(&nodesToDelete).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return e.NewWithCode(e.InternalError, errors.WithStack(err))
	}

	for _, n := range nodesToDelete {
		err := r.db.Where("node_from_id = ?", n.ID).Delete(models.Connection{}).Error
		if err != nil {
			return e.NewWithCode(e.InternalError, errors.WithStack(err))
		}
	}
	err = r.db.Where("graph_id = ?", graphID).Delete(&models.Result{}).Error
	if err != nil {
		return e.NewWithCode(e.InternalError, errors.WithStack(err))
	}
	for _, n := range nodesToDelete {
		err := r.db.Delete(&n).Error
		if err != nil {
			return e.NewWithCode(e.InternalError, errors.WithStack(err))
		}
	}
	err = r.db.Delete(&models.Graph{}, graphID).Error
	if err != nil {
		return e.NewWithCode(e.InternalError, errors.WithStack(err))
	}

	return nil

}

func (r *dataRepository) GetGraphByIDWithoutPreloading(graphID uint) (*models.Graph, error) {
	var graph *models.Graph
	err := r.db.First(&graph, graphID).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, e.NewWithCode(e.NotFound, errors.WithStack(err))
		}
		return nil, e.NewWithCode(e.InternalError, errors.WithStack(err))
	}
	return graph, nil
}
