package service

import (
	"context"
	"encoding/json"
	"strconv"

	"github.com/pkg/errors"
	e "gitlab.com/SzZawadzki/dijkstra/error"

	dcs "gitlab.com/SzZawadzki/dijkstra/dijkstra_cache/service"
	"gitlab.com/SzZawadzki/dijkstra/graph_data/models"
	data "gitlab.com/SzZawadzki/dijkstra/graph_data/repositories/data"
	todo "gitlab.com/SzZawadzki/dijkstra/graph_data/repositories/todo"
)

type graphDataService struct {
	data                 data.DataRepository
	todo                 todo.Todo
	dijkstraCacheService dcs.DijkstraCacheSerivce
}

type GraphDataService interface {
	CreateGraph(ctx context.Context, graph *models.Graph) (*models.Graph, error)
	GetGraphsForUser(ctx context.Context) ([]*models.Graph, error)
	GetGraphByID(ctx context.Context, graphID uint) (*models.Graph, error)
	UpdateGraph(ctx context.Context, graph *models.Graph) (*models.Graph, error)
	Deletegraph(ctx context.Context, graphID uint) error
	SendToSolve(ctx context.Context, graphID uint) error
	CancelSolving(ctx context.Context, graphID uint) error
	AddResultToGraph(ctx context.Context, graphID uint) error
}

func NewGraphDataService(data data.DataRepository, todo todo.Todo, dijkstraCacheService dcs.DijkstraCacheSerivce) GraphDataService {
	return &graphDataService{data, todo, dijkstraCacheService}
}

func (s *graphDataService) AddResultToGraph(ctx context.Context, graphID uint) error {
	graphIDString := strconv.Itoa(int(graphID))
	resultsJson, err := s.dijkstraCacheService.GetDataResult(context.Background(), graphIDString)
	if err != nil {
		return e.NewWithCode(e.InternalError, errors.WithStack(err))
	}
	var results []*models.Result
	err = json.Unmarshal([]byte(resultsJson), &results)
	if err != nil {
		return e.NewWithCode(e.InternalError, errors.WithStack(err))
	}
	err = s.data.AddResultToGraph(results)
	if err != nil {
		return err
	}
	return nil
}

func (s *graphDataService) CancelSolving(ctx context.Context, graphID uint) error {
	userID, err := getUserId(ctx)
	if err != nil {
		return err
	}
	graph, err := s.data.GetGraphByIDWithoutPreloading(graphID)
	if err != nil {
		return err
	}
	if graph.UserID != userID {
		return e.NewWithCode(e.UnAuth, errors.New("unauthenticated"))
	}
	graphIDString := strconv.Itoa(int(graphID))
	err = s.dijkstraCacheService.SetCancel(context.Background(), graphIDString)
	if err != nil {
		return e.NewWithCode(e.InternalError, errors.WithStack(err))
	}
	return nil
}

func (s *graphDataService) SendToSolve(ctx context.Context, graphID uint) error {
	//check if istnt send to solve yet
	//check if have nodes
	userID, err := getUserId(ctx)
	if err != nil {
		return err
	}
	graph, err := s.data.GetGraphByID(graphID)
	if err != nil {
		return err
	}
	if graph.UserID != userID {
		return e.NewWithCode(e.UnAuth, errors.New("unauthenticated"))
	}
	b, err := json.Marshal(graph)
	if err != nil {
		return e.NewWithCode(e.InternalError, errors.WithStack(err))
	}
	graphIDString := strconv.Itoa(int(graphID))
	err = s.dijkstraCacheService.SetDataGraph(context.TODO(), graphIDString, string(b))
	if err != nil {
		return e.NewWithCode(e.InternalError, errors.WithStack(err))
	}
	err = s.todo.Publish(graphIDString)
	if err != nil {
		return err
	}
	return nil
}

func (s *graphDataService) Deletegraph(ctx context.Context, graphID uint) error {
	userID, err := getUserId(ctx)
	if err != nil {
		return err
	}
	graph, err := s.data.GetGraphByIDWithoutPreloading(graphID)
	if err != nil {
		return err
	}
	if graph.UserID != userID {
		return e.NewWithCode(e.UnAuth, errors.New("unauthenticated"))
	}
	err = s.data.DeleteGraph(graphID)
	if err != nil {
		return err
	}
	return nil
}

func (s *graphDataService) UpdateGraph(ctx context.Context, graph *models.Graph) (*models.Graph, error) {
	//Check if is result
	userID, err := getUserId(ctx)
	if err != nil {
		return nil, err
	}
	if !graph.PrepareAndValid() {
		return nil, e.NewWithCode(e.UnprocessableEntity, errors.New("invalid graph"))
	}
	oldGraph, err := s.data.GetGraphByIDWithoutPreloading(graph.ID)
	if err != nil {
		return nil, err
	}
	if oldGraph.UserID != userID {
		return nil, e.NewWithCode(e.UnAuth, errors.New("unauthenticated"))
	}
	graph.UserID = userID
	err = s.data.UpdateGraph(graph)
	if err != nil {
		return nil, err
	}
	return graph, nil

}

func (s *graphDataService) GetGraphByID(ctx context.Context, graphID uint) (*models.Graph, error) {
	userID, err := getUserId(ctx)
	if err != nil {
		return nil, err
	}
	graph, err := s.data.GetGraphByID(graphID)
	if err != nil {
		return nil, err
	}
	if graph.UserID != userID {
		return nil, e.NewWithCode(e.UnAuth, errors.New("unauthenticated"))
	}
	return graph, nil
}

func (s *graphDataService) GetGraphsForUser(ctx context.Context) ([]*models.Graph, error) {
	userID, err := getUserId(ctx)
	if err != nil {
		return nil, err
	}
	graphs, err := s.data.GetGraphsForUser(userID)
	if err != nil {
		return nil, err
	}
	return graphs, nil
}

func (s *graphDataService) CreateGraph(ctx context.Context, graph *models.Graph) (*models.Graph, error) {
	userID, err := getUserId(ctx)
	if err != nil {
		return nil, err
	}
	if !graph.PrepareAndValid() {
		return nil, e.NewWithCode(e.UnprocessableEntity, errors.New("invalid graph"))
	}
	graph.UserID = userID
	err = s.data.CreateGraph(graph)
	if err != nil {
		return nil, err
	}
	return graph, nil
}

func getUserId(ctx context.Context) (uint, error) {
	userIDI := ctx.Value("userID")
	if userIDI == nil {
		return 0, e.NewWithCode(e.UnAuth, errors.New("unauthenticated"))
	}
	userID, ok := userIDI.(uint)
	if !ok {
		return 0, e.NewWithCode(e.UnAuth, errors.New("unauthenticated"))
	}
	return userID, nil
}
