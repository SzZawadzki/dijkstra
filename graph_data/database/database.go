package database

import (
	"fmt"

	"gitlab.com/SzZawadzki/dijkstra/graph_data/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func GetConn(username string, password string, dbHost string, dbName string) *gorm.DB {
	//err := godotenv.Load()
	// if err != nil {
	// 	fmt.Print(err)
	// }

	// username := os.Getenv("db_user")
	// password := os.Getenv("db_pass")
	// dbHost := os.Getenv("db_host")

	dbUri := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, username, dbName, password)
	fmt.Println(dbUri)

	conn, err := gorm.Open(postgres.Open(dbUri), &gorm.Config{})
	if err != nil {
		fmt.Print(err)
	}

	db := conn
	db.Debug().AutoMigrate(&models.Node{})
	db.Debug().AutoMigrate(&models.Connection{})
	db.Debug().AutoMigrate(&models.Graph{})
	db.Debug().AutoMigrate(&models.Result{})
	return db
}
