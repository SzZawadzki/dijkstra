package endpoints

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	"gitlab.com/SzZawadzki/dijkstra/dijkstra_cache/service"
)

type Set struct {
	SetCancelEndpoint     endpoint.Endpoint
	CheckCancelEndpoint   endpoint.Endpoint
	SetDataGraphEndpoint  endpoint.Endpoint
	GetDataGraphEndpoint  endpoint.Endpoint
	SetDataResultEndpoint endpoint.Endpoint
	GetDataResultEndpoint endpoint.Endpoint
}

func New(service service.DijkstraCacheSerivce) Set {
	return Set{
		MakeSetCancelEndpoint(service),
		MakeCheckCancelEndpoint(service),
		MakeSetDataGraphEndpoint(service),
		MakeGetDataGraphEndpoint(service),
		MakeSetDataResultEndpoint(service),
		MakeGetDataResultEndpoint(service),
	}
}

func (s Set) SetCancel(ctx context.Context, graphID string) error {
	resp, err := s.SetCancelEndpoint(ctx, SimpleRequest{GraphID: graphID})
	if err != nil {
		return err
	}
	response := resp.(SimpleResponse)
	return response.Err
}

func (s Set) CheckCancel(ctx context.Context, graphID string) (bool, error) {
	resp, err := s.SetCancelEndpoint(ctx, SimpleRequest{GraphID: graphID})
	if err != nil {
		return false, err
	}
	response := resp.(BoolResponse)
	return response.Data, response.Err
}

func (s Set) SetDataGraph(ctx context.Context, graphID string, json string) error {
	resp, err := s.SetDataGraphEndpoint(ctx, WithDataRequest{GraphID: graphID, Data: json})
	if err != nil {
		return err
	}
	response := resp.(SimpleResponse)
	return response.Err
}

func (s Set) GetDataGraph(ctx context.Context, graphID string) (string, error) {
	resp, err := s.GetDataGraphEndpoint(ctx, SimpleRequest{GraphID: graphID})
	if err != nil {
		return "", err
	}
	response := resp.(WithDataResponse)
	return response.Data, response.Err
}
func (s Set) SetDataResult(ctx context.Context, graphID string, json string) error {
	resp, err := s.SetDataResultEndpoint(ctx, WithDataRequest{GraphID: graphID, Data: json})
	if err != nil {
		return err
	}
	response := resp.(SimpleResponse)
	return response.Err
}
func (s Set) GetDataResult(ctx context.Context, graphID string) (string, error) {
	resp, err := s.GetDataResultEndpoint(ctx, SimpleRequest{GraphID: graphID})
	if err != nil {
		return "", err
	}
	response := resp.(WithDataResponse)
	return response.Data, response.Err
}

func MakeSetCancelEndpoint(s service.DijkstraCacheSerivce) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(SimpleRequest)
		err := s.SetCancel(ctx, req.GraphID)
		return SimpleResponse{Err: err}, nil
	}
}

func MakeCheckCancelEndpoint(s service.DijkstraCacheSerivce) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(SimpleRequest)
		v, err := s.CheckCancel(ctx, req.GraphID)
		return BoolResponse{Data: v, Err: err}, nil
	}
}

func MakeSetDataGraphEndpoint(s service.DijkstraCacheSerivce) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(WithDataRequest)
		err := s.SetDataGraph(ctx, req.GraphID, req.Data)
		return SimpleResponse{Err: err}, nil
	}
}

func MakeGetDataGraphEndpoint(s service.DijkstraCacheSerivce) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(SimpleRequest)
		v, err := s.GetDataGraph(ctx, req.GraphID)
		return WithDataResponse{Data: v, Err: err}, nil
	}
}

func MakeSetDataResultEndpoint(s service.DijkstraCacheSerivce) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(WithDataRequest)
		err := s.SetDataResult(ctx, req.GraphID, req.Data)
		return SimpleResponse{Err: err}, nil
	}
}

func MakeGetDataResultEndpoint(s service.DijkstraCacheSerivce) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(SimpleRequest)
		v, err := s.GetDataResult(ctx, req.GraphID)
		return WithDataResponse{Data: v, Err: err}, nil
	}
}

type SimpleRequest struct {
	GraphID string
}

type WithDataRequest struct {
	GraphID string
	Data    string
}

type SimpleResponse struct {
	Err error `json:"-"`
}

type WithDataResponse struct {
	Data string `json:"data"`
	Err  error  `json:"-"`
}

type BoolResponse struct {
	Data bool  `json:"data"`
	Err  error `json:"-"`
}

func (r SimpleResponse) Failed() error { return r.Err }

func (r WithDataResponse) Failed() error { return r.Err }

func (r BoolResponse) Failed() error { return r.Err }
