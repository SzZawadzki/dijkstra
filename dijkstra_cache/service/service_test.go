package service_test

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/go-redis/redismock/v8"
	"github.com/stretchr/testify/require"
	"gitlab.com/SzZawadzki/dijkstra/dijkstra_cache/service"
)

var cacheService service.DijkstraCacheSerivce
var ctx = context.TODO()
var dataMock redismock.ClientMock
var cancelMock redismock.ClientMock
var resultMock redismock.ClientMock

func TestMain(m *testing.M) {
	cancelClient, cMock := redismock.NewClientMock()
	cancelMock = cMock
	dataClient, dMock := redismock.NewClientMock()
	dataMock = dMock
	resultClient, rMock := redismock.NewClientMock()
	resultMock = rMock
	cacheService = service.NewDijkstraCacheService(cancelClient, dataClient, resultClient)
	os.Exit(m.Run())
}

func TestSetCancelOK(t *testing.T) {
	assertions := require.New(t)
	graphID := "343434"
	jsonGraph := "jsonRepresentationGraph"
	dataMock.ExpectGet(graphID).SetVal(jsonGraph)
	dataMock.ExpectDel(graphID).SetVal(0)
	cancelMock.ExpectSet(graphID, true, 24*time.Hour).SetVal("1")

	err := cacheService.SetCancel(ctx, graphID)

	assertions.NoError(err, "Set cancel error")
}

func TestSetCancelTooLate(t *testing.T) {
	assertions := require.New(t)
	graphID := "345454"
	dataMock.ExpectGet(graphID).RedisNil()

	err := cacheService.SetCancel(ctx, graphID)

	assertions.ErrorIs(err, service.ErrCannotCancel, "Cancel error")
}

func TestCheckCancelTrue(t *testing.T) {
	assertions := require.New(t)
	graphID := "5465656"
	cancelMock.ExpectGet(graphID).SetVal("true")

	v, err := cacheService.CheckCancel(ctx, graphID)

	assertions.NoError(err, "Check cancel error")

	assertions.Equal(true, v, "Cancel value")
}

func TestCheckCancelFalse(t *testing.T) {
	assertions := require.New(t)
	graphID := "5465656"
	cancelMock.ExpectGet(graphID).RedisNil()

	v, err := cacheService.CheckCancel(ctx, graphID)

	assertions.NoError(err, "Check cancel error")

	assertions.Equal(false, v, "Cancel value")
}

func TestSetDataGraph(t *testing.T) {
	assertions := require.New(t)
	graphID := "465765"
	jsonGraph := "jsonRepresentationGraph"
	dataMock.ExpectSet(graphID, jsonGraph, 24*time.Hour).SetVal(jsonGraph)

	err := cacheService.SetDataGraph(ctx, graphID, jsonGraph)

	assertions.NoError(err, "Set Data Graph Error")
}

func TestGetDataGraph(t *testing.T) {
	assertions := require.New(t)
	graphID := "7655"
	jsonGraph := "jsonRepresentationGraph"
	dataMock.ExpectGet(graphID).SetVal(jsonGraph)
	dataMock.ExpectDel(graphID).SetVal(0)

	v, err := cacheService.GetDataGraph(ctx, graphID)

	assertions.NoError(err, "Get Data Graph Error")

	assertions.Equal(jsonGraph, v, "Value")
}

func TestSetDataResult(t *testing.T) {
	assertions := require.New(t)
	graphID := "27533"
	jsonResult := "jsonRepresentationResult"
	resultMock.ExpectSet(graphID, jsonResult, 24*time.Hour).SetVal(jsonResult)

	err := cacheService.SetDataResult(ctx, graphID, jsonResult)

	assertions.NoError(err, "Set Data Result Error")
}

func TestGetDataResult(t *testing.T) {
	assertions := require.New(t)
	graphID := "84344"
	jsonResult := "jsonRepresentationResult"
	resultMock.ExpectGet(graphID).SetVal(jsonResult)
	resultMock.ExpectDel(graphID).SetVal(0)

	v, err := cacheService.GetDataResult(ctx, graphID)

	assertions.NoError(err, "Get Data Result Error")

	assertions.Equal(jsonResult, v, "Value")
}
