package service

import (
	"context"
	"errors"
	"log"
	"time"

	"github.com/go-redis/redis/v8"
)

type dijkstraCacheSerivce struct {
	cancelClient *redis.Client
	dataClient   *redis.Client
	resultClient *redis.Client
}

type DijkstraCacheSerivce interface {
	SetCancel(ctx context.Context, graphID string) error
	CheckCancel(ctx context.Context, graphID string) (bool, error)
	SetDataGraph(ctx context.Context, graphID string, json string) error
	GetDataGraph(ctx context.Context, graphID string) (string, error)
	SetDataResult(ctx context.Context, graphID string, json string) error
	GetDataResult(ctx context.Context, graphID string) (string, error)
}

var (
	ErrDatabaseProblem = errors.New("ErrorDatabaseProblem")
	ErrNoData          = errors.New("ErrorNoData")
	ErrCannotCancel    = errors.New("TooLatacannotCancel")
)

func NewDijkstraCacheService(cancelClient *redis.Client, dataClient *redis.Client, resultClient *redis.Client) DijkstraCacheSerivce {
	return &dijkstraCacheSerivce{cancelClient: cancelClient, dataClient: dataClient, resultClient: resultClient}
}

func (d *dijkstraCacheSerivce) SetCancel(ctx context.Context, graphID string) error {
	err := d.dataClient.Get(ctx, graphID).Err()
	if err == redis.Nil {
		return ErrCannotCancel
	} else if err != nil {
		return ErrDatabaseProblem
	}
	err = d.dataClient.Del(ctx, graphID).Err()
	if err != nil {
		return ErrDatabaseProblem
	}
	err = d.cancelClient.Set(ctx, graphID, true, 24*time.Hour).Err()
	if err != nil {
		log.Println("tak ", err)
		return ErrDatabaseProblem
	}
	return nil
}

func (d *dijkstraCacheSerivce) CheckCancel(ctx context.Context, graphID string) (bool, error) {
	val, err := d.cancelClient.Get(ctx, graphID).Bool()
	if err != nil {
		if err == redis.Nil {
			return false, nil
		}
		return false, ErrDatabaseProblem
	}
	return val, nil
}
func (d *dijkstraCacheSerivce) SetDataGraph(ctx context.Context, graphID string, json string) error {
	err := d.dataClient.Set(ctx, graphID, json, 24*time.Hour).Err()
	if err != nil {
		return ErrDatabaseProblem
	}
	return nil
}
func (d *dijkstraCacheSerivce) GetDataGraph(ctx context.Context, graphID string) (string, error) {
	val, err := d.dataClient.Get(ctx, graphID).Result()
	if err == redis.Nil {
		return "", ErrNoData
	} else if err != nil {
		return "", ErrDatabaseProblem
	}
	err = d.dataClient.Del(ctx, graphID).Err()
	if err != nil {
		return "", ErrDatabaseProblem
	}
	return val, nil
}
func (d *dijkstraCacheSerivce) SetDataResult(ctx context.Context, graphID string, json string) error {
	err := d.resultClient.Set(ctx, graphID, json, 24*time.Hour).Err()
	if err != nil {
		return ErrDatabaseProblem
	}
	return nil
}
func (d *dijkstraCacheSerivce) GetDataResult(ctx context.Context, graphID string) (string, error) {
	val, err := d.resultClient.Get(ctx, graphID).Result()
	if err == redis.Nil {
		return "", ErrNoData
	} else if err != nil {
		return "", ErrDatabaseProblem
	}
	err = d.resultClient.Del(ctx, graphID).Err()
	if err != nil {
		return "", ErrDatabaseProblem
	}
	return val, nil
}
