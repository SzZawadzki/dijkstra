package transport

import (
	"context"
	"errors"

	grpctransport "github.com/go-kit/kit/transport/grpc"
	e "gitlab.com/SzZawadzki/dijkstra/dijkstra_cache/endpoints"
	pb "gitlab.com/SzZawadzki/dijkstra/dijkstra_cache/pb"
	"gitlab.com/SzZawadzki/dijkstra/dijkstra_cache/service"
	"google.golang.org/grpc"
)

type dijkstraCacheGrpcServer struct {
	pb.UnimplementedDijkstraCacheServer
	setCancel     grpctransport.Handler
	checkCancel   grpctransport.Handler
	setDataGraph  grpctransport.Handler
	getDataGraph  grpctransport.Handler
	setDataResult grpctransport.Handler
	getDataResult grpctransport.Handler
}

func NewDijkstraCacheGRPCServer(set e.Set) pb.DijkstraCacheServer {
	return &dijkstraCacheGrpcServer{
		setCancel: grpctransport.NewServer(
			set.SetCancelEndpoint,
			decodeGRPCSimpleRequest,
			encodeGRPCSimpleResponse,
		),
		checkCancel: grpctransport.NewServer(
			set.CheckCancelEndpoint,
			decodeGRPCSimpleRequest,
			encodeGRPCBoolResponse,
		),
		setDataGraph: grpctransport.NewServer(
			set.SetDataGraphEndpoint,
			decodeGRPCWithDataRequest,
			encodeGRPCSimpleResponse,
		),
		getDataGraph: grpctransport.NewServer(
			set.GetDataGraphEndpoint,
			decodeGRPCSimpleRequest,
			encodeGRPCWithDataResponse,
		),
		setDataResult: grpctransport.NewServer(
			set.SetDataResultEndpoint,
			decodeGRPCWithDataRequest,
			encodeGRPCSimpleResponse,
		),
		getDataResult: grpctransport.NewServer(
			set.GetDataResultEndpoint,
			decodeGRPCSimpleRequest,
			encodeGRPCWithDataResponse,
		),
	}
}

func (s *dijkstraCacheGrpcServer) SetCancel(ctx context.Context, req *pb.SimpleRequest) (*pb.SimpleResponse, error) {
	_, resp, err := s.setCancel.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return resp.(*pb.SimpleResponse), nil
}

func (s *dijkstraCacheGrpcServer) CheckCancel(ctx context.Context, req *pb.SimpleRequest) (*pb.BoolResponse, error) {
	_, resp, err := s.checkCancel.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return resp.(*pb.BoolResponse), nil
}

func (s *dijkstraCacheGrpcServer) SetDataGraph(ctx context.Context, req *pb.WithDataRequest) (*pb.SimpleResponse, error) {
	_, resp, err := s.setDataGraph.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return resp.(*pb.SimpleResponse), nil
}

func (s *dijkstraCacheGrpcServer) GetDataGraph(ctx context.Context, req *pb.SimpleRequest) (*pb.WithDataResponse, error) {
	_, resp, err := s.getDataGraph.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return resp.(*pb.WithDataResponse), nil
}
func (s *dijkstraCacheGrpcServer) SetDataResult(ctx context.Context, req *pb.WithDataRequest) (*pb.SimpleResponse, error) {
	_, resp, err := s.setDataResult.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return resp.(*pb.SimpleResponse), nil
}
func (s *dijkstraCacheGrpcServer) GetDataResult(ctx context.Context, req *pb.SimpleRequest) (*pb.WithDataResponse, error) {
	_, resp, err := s.getDataResult.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return resp.(*pb.WithDataResponse), nil
}

func NewDijkstraCacheGRPCClient(conn *grpc.ClientConn) service.DijkstraCacheSerivce {
	var setCancelEndpoint = grpctransport.NewClient(
		conn,
		"pb.DijkstraCache",
		"SetCancel",
		encodeGRPCSimpleRequest,
		decodeGRPCSimpleResponse,
		pb.SimpleResponse{},
	).Endpoint()
	var checkCancelEndpoint = grpctransport.NewClient(
		conn,
		"pb.DijkstraCache",
		"CheckCancel",
		encodeGRPCSimpleRequest,
		decodeGRPCBoolResponse,
		pb.BoolResponse{},
	).Endpoint()
	var setDataGraphEndpoint = grpctransport.NewClient(
		conn,
		"pb.DijkstraCache",
		"SetDataGraph",
		encodeGRPCWithDataRequest,
		decodeGRPCSimpleResponse,
		pb.SimpleResponse{},
	).Endpoint()
	var getDataGraphEndpoint = grpctransport.NewClient(
		conn,
		"pb.DijkstraCache",
		"GetDataGraph",
		encodeGRPCSimpleRequest,
		decodeGRPCWithDataResponse,
		pb.WithDataResponse{},
	).Endpoint()
	var setDataResultEndpoint = grpctransport.NewClient(
		conn,
		"pb.DijkstraCache",
		"SetDataResult",
		encodeGRPCWithDataRequest,
		decodeGRPCSimpleResponse,
		pb.SimpleResponse{},
	).Endpoint()
	var getDataResultEndpoint = grpctransport.NewClient(
		conn,
		"pb.DijkstraCache",
		"GetDataResult",
		encodeGRPCSimpleRequest,
		decodeGRPCWithDataResponse,
		pb.WithDataResponse{},
	).Endpoint()

	return e.Set{
		SetCancelEndpoint:     setCancelEndpoint,
		CheckCancelEndpoint:   checkCancelEndpoint,
		SetDataGraphEndpoint:  setDataGraphEndpoint,
		GetDataGraphEndpoint:  getDataGraphEndpoint,
		SetDataResultEndpoint: setDataResultEndpoint,
		GetDataResultEndpoint: getDataResultEndpoint,
	}

}

func decodeGRPCSimpleRequest(ctx context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*pb.SimpleRequest)
	return e.SimpleRequest{GraphID: req.GraphID}, nil
}

func decodeGRPCWithDataRequest(ctx context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*pb.WithDataRequest)
	return e.WithDataRequest{GraphID: req.GraphID, Data: req.Data}, nil
}

func decodeGRPCSimpleResponse(ctx context.Context, grpcRes interface{}) (interface{}, error) {
	res := grpcRes.(*pb.SimpleResponse)
	return e.SimpleResponse{Err: str2err(res.Err)}, nil
}

func decodeGRPCWithDataResponse(ctx context.Context, grpcRes interface{}) (interface{}, error) {
	res := grpcRes.(*pb.WithDataResponse)
	return e.WithDataResponse{Data: res.Data, Err: str2err(res.Err)}, nil
}

func decodeGRPCBoolResponse(ctx context.Context, grpcRes interface{}) (interface{}, error) {
	res := grpcRes.(*pb.BoolResponse)
	return e.BoolResponse{Data: res.Data, Err: str2err(res.Err)}, nil
}

func encodeGRPCSimpleRequest(ctx context.Context, request interface{}) (interface{}, error) {
	req := request.(e.SimpleRequest)
	return &pb.SimpleRequest{GraphID: req.GraphID}, nil
}

func encodeGRPCWithDataRequest(ctx context.Context, request interface{}) (interface{}, error) {
	req := request.(e.WithDataRequest)
	return &pb.WithDataRequest{GraphID: req.GraphID, Data: req.Data}, nil
}

func encodeGRPCSimpleResponse(ctx context.Context, response interface{}) (interface{}, error) {
	res := response.(e.SimpleResponse)
	return &pb.SimpleResponse{Err: err2str(res.Err)}, nil
}

func encodeGRPCWithDataResponse(ctx context.Context, response interface{}) (interface{}, error) {
	res := response.(e.WithDataResponse)
	return &pb.WithDataResponse{Data: res.Data, Err: err2str(res.Err)}, nil
}

func encodeGRPCBoolResponse(ctx context.Context, response interface{}) (interface{}, error) {
	res := response.(e.BoolResponse)
	return &pb.BoolResponse{Data: res.Data, Err: err2str(res.Err)}, nil
}

func str2err(s string) error {
	if s == "" {
		return nil
	}
	return errors.New(s)
}

func err2str(err error) string {
	if err == nil {
		return ""
	}
	return err.Error()
}
