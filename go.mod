module gitlab.com/SzZawadzki/dijkstra

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-kit/kit v0.10.0
	github.com/go-redis/redis/v8 v8.9.0
	github.com/go-redis/redismock/v8 v8.0.6
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.10.2
	github.com/nbio/st v0.0.0-20140626010706-e9e8d9816f32 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/roylee0704/gron v0.0.0-20160621042432-e78485adab46
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
	golang.org/x/net v0.0.0-20210510120150-4163338589ed // indirect
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.26.0
	gopkg.in/h2non/gentleman.v2 v2.0.5
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.10
)
