package repositories

import (
	"errors"

	"github.com/jinzhu/gorm"
	"gitlab.com/SzZawadzki/dijkstra/auth/models"
)

type userRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &userRepository{db: db}
}

type UserRepository interface {
	GetByEmail(string) (*models.User, error)
	Create(*models.User) (*models.User, error)
}

func (us *userRepository) GetByEmail(email string) (*models.User, error) {
	user := &models.User{}
	err := us.db.Table("users").Where("email = ?", email).First(user).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Email address not found")
		}
		return nil, errors.New("Connection error. Please retry")
	}
	return user, nil
}

func (us *userRepository) Create(user *models.User) (*models.User, error) {
	us.db.Create(user)

	if user.ID <= 0 {
		return nil, errors.New("Problem with generating token")
	}
	return user, nil
}
