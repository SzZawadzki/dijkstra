package models

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

type JwtUserClaims struct {
	UserId uint
	jwt.StandardClaims
}

func NewJwtUserClaims(u *User, expiresAt time.Time, issuer string) (claims jwt.Claims) {
	sc := jwt.StandardClaims{ExpiresAt: expiresAt.Unix(), Issuer: issuer}
	claims = JwtUserClaims{UserId: u.ID, StandardClaims: sc}
	return
}
