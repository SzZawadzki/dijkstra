package service

import (
	"context"
	"crypto/rsa"

	"github.com/go-kit/kit/log"
)

type Middleware func(AuthService) AuthService

func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next AuthService) AuthService {
		return loggingMiddleware{logger, next}
	}
}

type loggingMiddleware struct {
	logger log.Logger
	next   AuthService
}

func (mv loggingMiddleware) SignIn(ctx context.Context, email string, password string) (s string, err error) {
	defer func() {
		mv.logger.Log("method", "SignIn", "err", err)
	}()
	return mv.next.SignIn(ctx, email, password)
}
func (mv loggingMiddleware) SignUp(ctx context.Context, email string, password string) (s string, err error) {
	defer func() {
		mv.logger.Log("method", "SignUp", "err", err)
	}()
	return mv.next.SignUp(ctx, email, password)
}
func (mv loggingMiddleware) LogOut(ctx context.Context) (err error) {
	defer func() {
		mv.logger.Log("method", "LogOut", "err", err)
	}()
	return mv.next.LogOut(ctx)
}
func (mv loggingMiddleware) Refresh(ctx context.Context, token string) (s string, err error) {
	defer func() {
		mv.logger.Log("method", "Refresh", "err", err)
	}()
	return mv.next.Refresh(ctx, token)
}
func (mv loggingMiddleware) GetPublicKey(ctx context.Context) (s rsa.PublicKey, err error) {
	defer func() {
		mv.logger.Log("method", "GetPublicKey", "err", err)
	}()
	return mv.next.GetPublicKey(ctx)
}
