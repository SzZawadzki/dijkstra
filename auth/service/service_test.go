package service_test

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/SzZawadzki/dijkstra/auth/models"
	"gitlab.com/SzZawadzki/dijkstra/auth/repositories/mocks"
	"gitlab.com/SzZawadzki/dijkstra/auth/service"
)

func TestGenereteTokenWithClaims(t *testing.T) {
	assertions := require.New(t)
	privateKey, _ := rsa.GenerateKey(rand.Reader, 2048)
	publicKey := privateKey.PublicKey

	us := new(mocks.UserRepository)

	service := service.NewAuthService(us, privateKey, &publicKey)

	user := &models.User{gorm.Model{ID: 12}, "test@test.com", "123"}

	us.On("Create", mock.Anything).Return(user, nil)

	token, err := service.SignUp(context.Background(), user.Email, user.Password)
	assertions.Nil(err, "signup error")

	claims := &models.JwtUserClaims{}

	jwtToken, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		return &publicKey, nil
	})

	assertions.Equal(true, jwtToken.Valid)
	assertions.Equal(claims.UserId, user.ID)
}
