package service

import (
	"context"
	"crypto/rsa"
	"errors"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/SzZawadzki/dijkstra/auth/models"
	"gitlab.com/SzZawadzki/dijkstra/auth/repositories"
	"golang.org/x/crypto/bcrypt"
)

type authService struct {
	userRepository repositories.UserRepository
	privateKey     *rsa.PrivateKey
	publicKey      *rsa.PublicKey
}

func NewAuthService(ur repositories.UserRepository, privateKey *rsa.PrivateKey, publicKey *rsa.PublicKey) AuthService {
	return &authService{userRepository: ur, privateKey: privateKey, publicKey: publicKey}
}

type AuthService interface {
	SignIn(ctx context.Context, email string, password string) (string, error)
	SignUp(ctx context.Context, email string, password string) (string, error)
	LogOut(ctx context.Context) error
	Refresh(ctx context.Context, token string) (string, error)
	GetPublicKey(ctx context.Context) (rsa.PublicKey, error)
}

var (
	ErrEmailPasswordCombination = errors.New("invalid email and password combination")

	ErrServer = errors.New("server error")

	ErrMalformedToken = errors.New("malformed authentication token")

	ErrExpiredToken = errors.New("expired token")
)

func (s *authService) SignIn(ctx context.Context, email string, password string) (string, error) {
	user, err := s.userRepository.GetByEmail(email)
	if err != nil {
		return "", ErrEmailPasswordCombination
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		return "", ErrEmailPasswordCombination
	}

	tokenString, err := s.generateToken(user)

	return tokenString, err
}

func (s *authService) SignUp(ctx context.Context, email string, password string) (string, error) {
	hashPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", ErrServer
	}

	user := &models.User{Email: email, Password: string(hashPassword)}

	user, err = s.userRepository.Create(user)
	if err != nil {
		return "", ErrServer
	}

	tokenString, err := s.generateToken(user)

	return tokenString, err
}

func (s *authService) LogOut(ctx context.Context) error {
	return nil
}

func (s *authService) Refresh(ctx context.Context, tokenString string) (string, error) {
	claims := &models.JwtUserClaims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(t *jwt.Token) (interface{}, error) {
		return s.publicKey, nil
	})

	if err != nil {
		return "", ErrMalformedToken
	}

	if !token.Valid {
		return "", ErrMalformedToken
	}

	if claims.ExpiresAt < time.Now().Unix() {
		return "", ErrExpiredToken
	}

	claims.ExpiresAt = time.Now().Local().Add(time.Minute * time.Duration(5)).Unix()

	return s.generateTokenWithClaims(claims)
}

func (s *authService) GetPublicKey(ctx context.Context) (rsa.PublicKey, error) {
	return *s.publicKey, nil
}

func (s *authService) generateToken(user *models.User) (string, error) {
	expTime := time.Now().Local().Add(time.Minute * time.Duration(5))
	claims := models.NewJwtUserClaims(user, expTime, "auth")
	return s.generateTokenWithClaims(claims)
}

func (s *authService) generateTokenWithClaims(claims jwt.Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	tokenString, err := token.SignedString(s.privateKey)
	if err != nil {
		return "", ErrServer
	}
	return tokenString, nil
}
