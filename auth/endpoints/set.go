package endpoints

import (
	"context"
	"crypto/rsa"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"gitlab.com/SzZawadzki/dijkstra/auth/service"
)

type Set struct {
	SignInEndpoint       endpoint.Endpoint
	SignUpEndpoint       endpoint.Endpoint
	LogOutEndpoint       endpoint.Endpoint
	RefreshEndpoint      endpoint.Endpoint
	GetPublicKeyEndpoint endpoint.Endpoint
}

func New(as service.AuthService, logger log.Logger) Set {
	return Set{
		SignInEndpoint:       makeSignInEndpoint(as),
		SignUpEndpoint:       makeSignUpEndpoint(as),
		LogOutEndpoint:       makeLogOutEndpoint(as),
		RefreshEndpoint:      makeRefreshEndpoint(as),
		GetPublicKeyEndpoint: makeGetPublicKeyEndpoint(as),
	}
}

func (s *Set) SignIn(ctx context.Context, email string, password string) (string, error) {
	resp, err := s.SignInEndpoint(ctx, SignInRequest{Email: email, Password: password})
	if err != nil {
		return "", err
	}
	response := resp.(SignInResponse)
	return response.Token, response.Err
}

func (s *Set) SignUp(ctx context.Context, email string, password string) (string, error) {
	resp, err := s.SignUpEndpoint(ctx, SignUpRequest{Email: email, Password: password})
	if err != nil {
		return "", err
	}
	response := resp.(SignUpResponse)
	return response.Token, response.Err
}

func (s *Set) LogOut(ctx context.Context) error {
	resp, err := s.LogOutEndpoint(ctx, LogOutRequest{})
	if err != nil {
		return err
	}
	response := resp.(LogOutResponse)
	return response.Err
}

func (s *Set) Refresh(ctx context.Context, tokenString string) (string, error) {
	resp, err := s.RefreshEndpoint(ctx, RefreshRequest{Token: tokenString})
	if err != nil {
		return "", err
	}
	response := resp.(RefreshResponse)
	return response.Token, response.Err
}

func (s *Set) GetPublicKey(ctx context.Context) (rsa.PublicKey, error) {
	resp, err := s.GetPublicKeyEndpoint(ctx, GetPublicKeyRequest{})
	if err != nil {
		return rsa.PublicKey{}, err
	}
	response := resp.(GetPublicKeyResponse)
	return response.PublicKey, response.Err
}

func makeSignInEndpoint(s service.AuthService) endpoint.Endpoint {
	return func(ctx context.Context, reqest interface{}) (interface{}, error) {
		req := reqest.(SignInRequest)
		token, err := s.SignIn(ctx, req.Email, req.Password)
		return SignInResponse{Token: token, Err: err}, nil
	}
}

func makeSignUpEndpoint(s service.AuthService) endpoint.Endpoint {
	return func(ctx context.Context, reqest interface{}) (interface{}, error) {
		req := reqest.(SignUpRequest)
		token, err := s.SignUp(ctx, req.Email, req.Password)
		return SignUpResponse{Token: token, Err: err}, nil
	}
}

func makeLogOutEndpoint(s service.AuthService) endpoint.Endpoint {
	return func(ctx context.Context, reqest interface{}) (interface{}, error) {
		//req := reqest.(LogOutRequest)
		err := s.LogOut(ctx)
		return LogOutResponse{Err: err}, nil
	}
}

func makeRefreshEndpoint(s service.AuthService) endpoint.Endpoint {
	return func(ctx context.Context, reqest interface{}) (interface{}, error) {
		req := reqest.(RefreshRequest)
		newToken, err := s.Refresh(ctx, req.Token)
		return RefreshResponse{Token: newToken, Err: err}, nil
	}
}

func makeGetPublicKeyEndpoint(s service.AuthService) endpoint.Endpoint {
	return func(ctx context.Context, reqest interface{}) (interface{}, error) {
		//req := reqest.(LogOutRequest)
		publicKey, err := s.GetPublicKey(ctx)
		return GetPublicKeyResponse{PublicKey: publicKey, Err: err}, nil
	}
}

type SignInRequest struct {
	Email    string
	Password string
}

type SignInResponse struct {
	Token string
	Err   error
}

func (r SignInResponse) Failed() error {
	return r.Err
}

type SignUpRequest struct {
	Email    string
	Password string
}

type SignUpResponse struct {
	Token string
	Err   error
}

func (r SignUpResponse) Failed() error {
	return r.Err
}

type LogOutRequest struct {
}

type LogOutResponse struct {
	Err error
}

func (r LogOutResponse) Failed() error {
	return r.Err
}

type RefreshRequest struct {
	Token string
}

type RefreshResponse struct {
	Token string
	Err   error
}

func (r RefreshResponse) Failed() error {
	return r.Err
}

type GetPublicKeyRequest struct {
}

type GetPublicKeyResponse struct {
	PublicKey rsa.PublicKey
	Err       error
}

func (r GetPublicKeyResponse) Failed() error {
	return r.Err
}
