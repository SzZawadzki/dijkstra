package transport

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/transport"
	httptransport "github.com/go-kit/kit/transport/http"
	"gitlab.com/SzZawadzki/dijkstra/auth/endpoints"
	"gitlab.com/SzZawadzki/dijkstra/auth/service"
)

var (
	ErrInvalidBody  = errors.New("invalid body reqest")
	ErrInvalidToken = errors.New("invalid token")
)

func NewHttpHandler(endpoints endpoints.Set, logger log.Logger) http.Handler {
	options := []httptransport.ServerOption{
		httptransport.ServerErrorEncoder(errorEncoder),
		httptransport.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
	}

	m := http.NewServeMux()
	m.Handle("/signin", httptransport.NewServer(
		endpoints.SignInEndpoint,
		decodeHttpSignInRequest,
		encodeHTTPSignInResponse,
		options...,
	))
	m.Handle("/signup", httptransport.NewServer(
		endpoints.SignUpEndpoint,
		decodeHttpSignUpRequest,
		encodeHTTPSignUpResponse,
		options...,
	))
	m.Handle("/logout", httptransport.NewServer(
		endpoints.LogOutEndpoint,
		decodeHttpLogOutRequest,
		encodeHTTPGenericResponse,
		options...,
	))
	m.Handle("/refresh", httptransport.NewServer(
		endpoints.RefreshEndpoint,
		decodeHttpRefreshRequest,
		encodeHTTPRefreshResponse,
		options...,
	))
	m.Handle("/publickey", httptransport.NewServer(
		endpoints.GetPublicKeyEndpoint,
		decodeHttpGetPublicKeyRequest,
		encodeHTTPGenericResponse,
		options...,
	))

	return m
}

func errorEncoder(_ context.Context, err error, w http.ResponseWriter) {
	w.WriteHeader(err2code(err))
	json.NewEncoder(w).Encode(errorWrapper{Error: err.Error()})
}

func err2code(err error) int {
	switch err {
	case ErrInvalidBody, service.ErrEmailPasswordCombination, service.ErrMalformedToken, service.ErrExpiredToken:
		return http.StatusBadRequest
	}
	return http.StatusInternalServerError
}

func errorDecoder(r *http.Response) error {
	var w errorWrapper
	if err := json.NewDecoder(r.Body).Decode(&w); err != nil {
		return err
	}
	return errors.New(w.Error)
}

type errorWrapper struct {
	Error string `json:"error"`
}

func decodeHttpSignInRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req endpoints.SignInRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		err = ErrInvalidBody
	}
	return req, err
}

func decodeHttpSignUpRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req endpoints.SignUpRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		err = ErrInvalidBody
	}
	return req, err
}

func decodeHttpLogOutRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return endpoints.LogOutRequest{}, nil
}

func decodeHttpRefreshRequest(_ context.Context, r *http.Request) (interface{}, error) {
	cookie, err := r.Cookie("jwt")
	if err != nil {
		return nil, ErrInvalidToken
	}
	req := endpoints.RefreshRequest{Token: cookie.Value}
	return req, nil
}

func decodeHttpGetPublicKeyRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return endpoints.GetPublicKeyRequest{}, nil
}

func encodeHTTPSignInResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if f, ok := response.(endpoint.Failer); ok && f.Failed() != nil {
		errorEncoder(ctx, f.Failed(), w)
		return nil
	}
	resp := response.(endpoints.SignInResponse)
	cookie := &http.Cookie{Name: "jwt", Value: resp.Token}
	http.SetCookie(w, cookie)
	w.WriteHeader(http.StatusNoContent)
	return nil
}

func encodeHTTPSignUpResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if f, ok := response.(endpoint.Failer); ok && f.Failed() != nil {
		errorEncoder(ctx, f.Failed(), w)
		return nil
	}
	resp := response.(endpoints.SignUpResponse)
	cookie := &http.Cookie{Name: "jwt", Value: resp.Token}
	http.SetCookie(w, cookie)
	w.WriteHeader(http.StatusNoContent)
	return nil
}

func encodeHTTPRefreshResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if f, ok := response.(endpoint.Failer); ok && f.Failed() != nil {
		errorEncoder(ctx, f.Failed(), w)
		return nil
	}
	resp := response.(endpoints.RefreshResponse)
	cookie := &http.Cookie{Name: "jwt", Value: resp.Token}
	http.SetCookie(w, cookie)
	w.WriteHeader(http.StatusNoContent)
	return nil
}

func encodeHTTPGenericResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if f, ok := response.(endpoint.Failer); ok && f.Failed() != nil {
		errorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}
