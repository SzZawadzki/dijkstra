package error

type errorCode struct {
	text   string
	status int
}

var (
	UnAuth              = errorCode{"unauthenticated", 401}
	NotFound            = errorCode{"not-found", 404}
	InternalError       = errorCode{"internal-error", 500}
	UnprocessableEntity = errorCode{"unprocessable-entity", 422}
	BadRequest          = errorCode{"bad-request", 400}
)

type Error struct {
	Code  errorCode
	Cause error
}

func (e *Error) Error() string {
	return "Error code " + e.Code.text + ": " + e.Cause.Error()
}

func NewWithCode(code errorCode, cause error) error {
	return &Error{Code: code, Cause: cause}
}
