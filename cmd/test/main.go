package main

import (
	"log"
	"net/http"
	"strconv"

	validJwt "gitlab.com/SzZawadzki/dijkstra/valid_jwt"
)

type Pom struct {
	v validJwt.ValidJwt
}

func main() {
	v := validJwt.New("http://localhost:8080")
	v.Start()
	p := &Pom{v}
	http.HandleFunc("/welcome", p.getWelcome())
	log.Fatal(http.ListenAndServe(":8079", nil))
}

func (p *Pom) getWelcome() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		cookie, err := req.Cookie("jwt")
		if err != nil {
			w.Write([]byte("welcome"))
			return
		}
		jwt := cookie.Value
		userID, err := p.v.Valid(jwt)
		if err != nil {
			w.Write([]byte("welcome " + err.Error()))
			return
		}

		w.Write([]byte("welcome with auth id:" + strconv.Itoa(int(userID))))
	}
}
