package main

import (
	"crypto/rand"
	"crypto/rsa"
	"net/http"
	"os"

	"github.com/go-kit/kit/log"
	"gitlab.com/SzZawadzki/dijkstra/auth/database"
	"gitlab.com/SzZawadzki/dijkstra/auth/endpoints"
	"gitlab.com/SzZawadzki/dijkstra/auth/repositories"
	"gitlab.com/SzZawadzki/dijkstra/auth/service"
	"gitlab.com/SzZawadzki/dijkstra/auth/transport"
)

func main() {
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		panic(err)
	}
	publicKey := privateKey.PublicKey
	db := database.GetConn("dijkstra_user")
	repo := repositories.NewUserRepository(db)
	service := service.NewAuthService(repo, privateKey, &publicKey)

	logger := log.NewLogfmtLogger(os.Stderr)

	set := endpoints.New(service, logger)

	handler := transport.NewHttpHandler(set, logger)

	logger.Log("msg", "HTTP", "addr", ":8080")
	logger.Log("err", http.ListenAndServe(":8080", handler))
}
