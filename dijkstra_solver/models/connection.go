package models

type Connection struct {
	ID    uint
	From  *Node
	To    *Node
	Value int
}
