package models

type Graph struct {
	ID        uint
	Nodes     []*Node
	StartNode *Node
	IsResult  bool
	Result    map[*Node]int
}
