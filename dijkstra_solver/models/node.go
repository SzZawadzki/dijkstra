package models

type Node struct {
	ID          uint
	Name        string
	Connections []*Connection
}
