package solver_test

import (
	"sort"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/SzZawadzki/dijkstra/dijkstra_solver/models"
	"gitlab.com/SzZawadzki/dijkstra/dijkstra_solver/solver"
)

func TestPriorityQueueTest(t *testing.T) {
	assertions := require.New(t)
	qp := solver.NewPiorityQueue()

	values := []int{12, 25, 6, 5, 4}
	qp.Push(&models.Node{}, 12)
	qp.Push(&models.Node{}, 25)
	qp.Push(&models.Node{}, 6)
	qp.Push(&models.Node{}, 5)
	qp.Push(&models.Node{}, 4)
	sort.Ints(values)
	for i, v := range values {
		assertions.Equal(v, values[i], "Sould be equal")
	}
}

func TestSolver(t *testing.T) {
	assertions := require.New(t)
	goodResult := []int{0, 4, 8, 10, 10, 13, 13}

	n1 := &models.Node{ID: 1, Name: "1"}
	n2 := &models.Node{ID: 2, Name: "2"}
	n3 := &models.Node{ID: 3, Name: "3"}
	n4 := &models.Node{ID: 4, Name: "4"}
	n5 := &models.Node{ID: 5, Name: "5"}
	n6 := &models.Node{ID: 6, Name: "6"}
	n7 := &models.Node{ID: 7, Name: "7"}
	c12 := &models.Connection{12, n1, n2, 4}
	c21 := &models.Connection{21, n2, n1, 5}
	c13 := &models.Connection{13, n1, n3, 8}
	c34 := &models.Connection{34, n3, n4, 2}
	c36 := &models.Connection{36, n3, n6, 8}
	c25 := &models.Connection{25, n2, n5, 6}
	c47 := &models.Connection{47, n4, n7, 3}
	c45 := &models.Connection{45, n4, n5, 4}
	c53 := &models.Connection{53, n5, n3, 3}
	c56 := &models.Connection{56, n5, n6, 3}
	c62 := &models.Connection{62, n6, n2, 10}
	c72 := &models.Connection{72, n7, n2, 8}
	c76 := &models.Connection{76, n7, n7, 4}
	n1.Connections = []*models.Connection{c12, c13}
	n2.Connections = []*models.Connection{c21, c25}
	n3.Connections = []*models.Connection{c34, c36}
	n4.Connections = []*models.Connection{c45, c47}
	n5.Connections = []*models.Connection{c53, c56}
	n6.Connections = []*models.Connection{c62}
	n7.Connections = []*models.Connection{c72, c76}
	graph := &models.Graph{ID: 1, Nodes: []*models.Node{n1, n2, n3, n4, n5, n6, n7}, StartNode: n1}

	solver.Solve(graph)

	for i, r := range graph.Result {
		assertions.Equal(goodResult[i.ID-1], r, "Sould be equal")
	}
}
