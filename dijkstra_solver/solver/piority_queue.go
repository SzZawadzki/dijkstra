package solver

import (
	"container/heap"

	"gitlab.com/SzZawadzki/dijkstra/dijkstra_solver/models"
)

type item struct {
	node     *models.Node
	priority int
	index    int
}

type arrayQueue []*item

func (pq arrayQueue) Len() int { return len(pq) }

func (pq arrayQueue) Less(i, j int) bool {
	return pq[i].priority < pq[j].priority
}

func (pq arrayQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *arrayQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*item)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *arrayQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil
	item.index = -1
	*pq = old[0 : n-1]
	return item
}

func (pq *arrayQueue) update(item *item, node *models.Node, priority int) {
	item.node = node
	item.priority = priority
	heap.Fix(pq, item.index)
}

type priorityQueue struct {
	pq arrayQueue
}

type PriorityQueue interface {
	Push(node *models.Node, value int)
	Pop() (*models.Node, int)
	Empty() bool
}

func NewPiorityQueue() PriorityQueue {
	return &priorityQueue{make(arrayQueue, 0)}
}

func (pq *priorityQueue) Push(node *models.Node, value int) {
	it := &item{node, value, 1}
	pq.pq.Push(it)
}

func (pq *priorityQueue) Pop() (*models.Node, int) {
	it := pq.pq.Pop().(*item)
	return it.node, it.priority
}

func (pq *priorityQueue) Empty() bool {
	return pq.pq.Len() == 0
}
