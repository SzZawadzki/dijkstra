package solver

import "gitlab.com/SzZawadzki/dijkstra/dijkstra_solver/models"

func Solve(graph *models.Graph) error {
	visited := make(map[*models.Node]bool)
	distance := make(map[*models.Node]int)
	pq := NewPiorityQueue()

	for _, n := range graph.Nodes {
		distance[n] = -1
		visited[n] = false
	}
	pq.Push(graph.StartNode, 0)
	distance[graph.StartNode] = 0
	for !pq.Empty() {
		node, value := pq.Pop()
		if visited[node] && distance[node] < value {
			continue
		}
		visited[node] = true
		for _, c := range node.Connections {
			if value+c.Value < distance[c.To] || distance[c.To] == -1 {
				distance[c.To] = value + c.Value
				pq.Push(c.To, value+c.Value)
			}
		}
	}

	graph.IsResult = true
	graph.Result = distance

	return nil
}
