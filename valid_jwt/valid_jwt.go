package validJwt

import (
	"crypto/rsa"
	"errors"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/roylee0704/gron"
	"github.com/roylee0704/gron/xtime"
	ae "gitlab.com/SzZawadzki/dijkstra/auth/endpoints"
	am "gitlab.com/SzZawadzki/dijkstra/auth/models"
	"gopkg.in/h2non/gentleman.v2"
)

type validJwt struct {
	cron      *gron.Cron
	publicKey *rsa.PublicKey
	cli       *gentleman.Client
}

type ValidJwt interface {
	Start()
	Valid(tokenString string) (userID uint, err error)
}

var (
	ErrKeyProblem   = errors.New("problem with getting public key")
	ErrInvalidToken = errors.New("invalid token")
	ErrExpiredToken = errors.New("token is expired")
)

func New(authServerURL string) ValidJwt {
	cli := gentleman.New()
	cli.URL(authServerURL)
	v := &validJwt{gron.New(), nil, cli}
	everyFiveMinute := gron.Every(5 * xtime.Minute)
	v.cron.AddFunc(everyFiveMinute, v.checkPublicKey())
	return v
}

func (v *validJwt) Start() {
	v.checkPublicKey()()
	v.cron.Start()
}

func (v *validJwt) Valid(tokenString string) (userID uint, err error) {
	if v.publicKey == nil {
		return 0, ErrKeyProblem
	}

	claims := &am.JwtUserClaims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, v.keyFunc())
	if err != nil {
		return 0, ErrInvalidToken
	}
	if !token.Valid {
		return 0, ErrInvalidToken
	}

	if claims.ExpiresAt < time.Now().Unix() {
		return 0, ErrExpiredToken
	}

	return claims.UserId, nil
}

func (v *validJwt) keyFunc() jwt.Keyfunc {
	return func(token *jwt.Token) (interface{}, error) {
		return v.publicKey, nil
	}
}

func (v *validJwt) checkPublicKey() func() {
	return func() {
		req := v.cli.Request()
		req.Path("/publickey")
		resp, err := req.Send()
		if err != nil {
			//log
			fmt.Println(err.Error())
			return
		}
		if !resp.Ok {
			//log
			fmt.Println(resp.Error.Error())
			return
		}

		respObject := &ae.GetPublicKeyResponse{}

		err = resp.JSON(respObject)
		if err != nil {
			//log
			fmt.Println(err.Error())
			return
		}

		v.publicKey = &respObject.PublicKey
		fmt.Println(v.publicKey)
	}
}
