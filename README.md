# Dijkstra
A training project created to improve skill in programing microservices in Golang. Dijkstra is my favourite algorithm, and I don't see any online tools to solve our graph.

## Sections
- [Preview](#preview)
- [Flow](#flow)
- [Technologies](#technologies)
- [DONE](#done)
- [In progress](#in-progress)

## Previev
![Graph_DB](/uploads/55ad8e5ecb27b87361e43e0755b3f5b4/Graph_DB.jpg)

### Auth
Manage user and authorization, shore jwt in session.
### ValidJWT
Getting public key from Auth and authenticate users.
### GraphData 
Store Graph and solution.
### Cache Cancel
When graph was send to solve and before Dijkstra start solving it checks if user change his mind.
### Cache Data
Stored graph data to solver and solved results.
### TODO
channel to sent graph IDs to solve.
### DONE
channel to sent solved graph IDs. 

## **Flow**
- user create graph
- user apply to solve
- GraphData microservices sent graphID to "TODO"
- Dijkstra microservice check if not canceled
- get data from Cache Data
- solve graph
- push result to Cahce Data
- sent graph ID to channel "DONE"

## Technologies
- Golang 1.16
- RabbitMQ
- Postgres
- GO-kit

## DONE
- Auth service
- Dijkstra Cache service
- ValidJWT
- Dijkstra solver

## In progress
- Graph Data repository
